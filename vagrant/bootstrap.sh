#!/usr/bin/env bash

# This script installs all necessary packages to build DUNE-FemPy.

#Make sure that script exits on failure, and that all commands are printed
set -e
set -x

SUDO=
if [ "$UID" != "0" ]; then
  echo "Using sudo for installation!"
  SUDO=sudo
fi

# Make sure we have updated URLs to packages etc.
$SUDO apt-get update -y

$SUDO apt-get -y update \
   && apt-get install -y \
      apt-utils ca-certificates pkg-config software-properties-common \
      patch git git-lfs vim gnuplot gosu unzip gzip tar wget time sudo \
      build-essential gfortran gcc g++ gdb libmpich-dev cmake cmake-curses-gui \
      python3-venv python3-dev python3-setuptools python3-pip python3-wheel \
      python3-appdirs python3-configobj python3-requests python3-pandocfilters python3-tk \
      libsuitesparse-dev libtet1.5-dev gmsh paraview-python bash-completion valgrind \
      libboost-system-dev libboost-regex-dev libboost-filesystem-dev libboost-test-dev libboost-date-time-dev \
      gedit emacs \
   && apt-get remove -y libopenmpi2 openmpi-common \
   && apt-get autoremove -y \
   && apt-get upgrade -y \
   && rm -rf /var/lib/apt/lists/*
