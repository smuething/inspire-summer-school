#!/bin/bash

#PYENVDIR=$HOME

PYENVDIR=`pwd`
echo $PYENVDIR

DUNECOREMODULES="dune-common dune-istl dune-geometry dune-grid dune-localfunctions"
DUNEEXTMODULES="dune-python dune-alugrid dune-foamgrid"
DUNESUBMODULES="dune-subgrid"
DUNEFEMMODULES="dune-fem dune-fempy dune-fem-dg dune-vem"
#DUNEPDELABMODULES="dune-pdelab dune-pdelab-tutorials"
DUNEPDELABMODULES="dune-pdelab"

if [ "$DUNEPDELABMODULES" != "" ]; then
  DUNEEXTMODULES="$DUNEEXTMODULES dune-typetree dune-functions dune-uggrid"
fi

if [ "$DUNEVERSION" != "" ] ; then
  DUNEBRANCH="-b releases/$DUNEVERSION"
fi

# only clone with the last commit
GITCLONE="git clone --depth=50"

# create necessary python virtual environment
if ! test -d $PYENVDIR/dune-env ; then
  # do not use python3 here or Dune will not pick up virtualenv correctly
  python3.6 -m venv $PYENVDIR/dune-env
  source $PYENVDIR/dune-env/bin/activate

  pip install --upgrade pip
  pip install ufl numpy matplotlib scipy ipython mpi4py petsc4py pygmsh pandas
  pip install jupyter sphinx sphinx_rtd_theme apidoc sphinxcontrib-bibtex nbsphinx

  echo "\
source $PYENVDIR/dune-env/bin/activate
export DUNE_LOG_FORMAT='%(asctime)s - %(name)s - %(levelname)s - %(message)s'
export DUNE_LOG_LEVEL=CRITICAL

#####################################################################################

# function for reconfiguring all dune-fem related modules
function updateFem {
  DUNEFEMMODULES=\"dune-fem dune-fem-dg dune-vem\"
  thisDir=\$PWD
  cd ~/DUNE
  for M in \$DUNEFEMMODULES ; do
    ./dune-common/bin/dunecontrol --only=\$M git pull
  done
  cd dune-fempy
  git pull
  cd ..
  ./dune-common/bin/dunecontrol --opts=config.opts all
  cd \$thisDir
}
# function for updating all Dune modules
function updateDune {
  thisDir=\$PWD
  cd ~/DUNE
  ./dune-common/bin/dunecontrol git pull
  ./dune-common/bin/dunecontrol --opts=config.opts all
  cd \$thisDir
}
# function for updating the Dune Python environment
# warning: calling this function removes all the cached Python modules in # dune-py
function updatePython {
  thisDir=\$PWD
  cd ~/DUNE
  ./dune-python/bin/setup-dunepy.py --opts=config.opts install
  cd \$thisDir
}
# function for rebuilding the full Dune stack, including rebuilding the
# Dune modules and the Dune Python environment (also clears the dune-py # cache)
function updateAll {
  updateDune
  updatePython
}

export PYTHONPATH=\$PYTHONPATH:\$HOME/PorePy/python

#cd /host
" >> .bashrc

# page up and page down searches bash history
echo "
\$include /etc/inputrc

TAB: complete

# map \"page up\" and \"page down\" to search history based on current cmdline
\"\e[5~\": history-search-backward
\"\e[6~\": history-search-forward
" > .inputrc

else
  source $PYENVDIR/dune-env/bin/activate
fi

#change appropriately, i.e. 2.6 or empty which refers to master
DUNEVERSION=

FLAGS="-O3 -DNDEBUG -funroll-loops -finline-functions -Wall -ftree-vectorize -fno-stack-protector -mtune=native"

#PY_CXXFLAGS=`python3-config --includes`
FLAGS="$FLAGS $PY_CXXFLAGS"

#PY_LDFLAGS=`python3-config --ldflags`

if ! test -d DUNE ; then
# we need to put the dune module into a subdirectory otherwise dune-py in
# the virtual env will be picked up during build of dune
mkdir DUNE
cd DUNE
# build flags for all DUNE and OPM modules
# change according to your needs
echo "\
DUNEPATH=`pwd`
PYENVDIR=$PYENVDIR
BUILDDIR=build-cmake
USE_CMAKE=yes
MAKE_FLAGS=-j4
CMAKE_FLAGS=\"-DCMAKE_CXX_FLAGS=\\\"$FLAGS\\\"  \\
 -DCMAKE_LD_FLAGS=\\\"$PY_LDFLAGS\\\" \\
 -DALLOW_CXXFLAGS_OVERWRITE=ON \\
 -DENABLE_HEADERCHECK=OFF \\
 -DCMAKE_POSITION_INDEPENDENT_CODE=TRUE \\
 -DDUNE_PYTHON_INSTALL_EDITABLE=TRUE \\
 -DADDITIONAL_PIP_PARAMS="-upgrade" \\
 -DPETSC_DIR=\$PYENVDIR/dune-env/lib/python3.6/site-packages/petsc \\
 -DDUNE_PYTHON_FORCE_PYTHON_VERSION=3.6 \\
 -DDISABLE_DOCUMENTATION=TRUE \\
 -DCMAKE_DISABLE_FIND_PACKAGE_LATEX=TRUE\" " > config.opts

# get all dune modules necessary
for MOD in $DUNECOREMODULES ; do
  $GITCLONE $DUNEBRANCH https://gitlab.dune-project.org/core/$MOD.git
done

# get all dune extension modules necessary
for MOD in $DUNEEXTMODULES ; do
  if [ "$MOD" == "dune-foamgrid" ]; then
    $GITCLONE $DUNEBRANCH https://gitlab.dune-project.org/extensions/$MOD.git
  elif [ "$MOD" == "dune-alugrid" ]; then
    $GITCLONE $DUNEBRANCH https://gitlab.dune-project.org/extensions/$MOD.git
  else
    $GITCLONE $DUNEBRANCH https://gitlab.dune-project.org/staging/$MOD.git
  fi
done

# get all dune pdelab modules necessary
for MOD in $DUNESUBMODULES ; do
  $GITCLONE $DUNEBRANCH https://git.imp.fu-berlin.de/agnumpde/$MOD.git
done

# get all dune pdelab modules necessary
for MOD in $DUNEPDELABMODULES ; do
  $GITCLONE $DUNEBRANCH https://gitlab.dune-project.org/pdelab/$MOD.git
done

# get all dune extension modules necessary
for MOD in $DUNEFEMMODULES ; do
  if [ "$MOD" == "dune-fem-dg" ]; then
    $GITCLONE https://gitlab.dune-project.org/dune-fem/$MOD.git
  elif [ "$MOD" == "dune-fem" ]; then
    $GITCLONE https://gitlab.dune-project.org/dune-fem/$MOD.git
  elif [ "$MOD" == "dune-fempy" ]; then
    $GITCLONE https://gitlab.dune-project.org/dune-fem/$MOD.git
  else
    $GITCLONE $DUNEBRANCH https://gitlab.dune-project.org/dune-fem/$MOD.git
  fi
done

else
cd DUNE
./dune-common/bin/dunecontrol git pull
rm -rf */build-cmake
fi

# build all DUNE modules using dune-control
./dune-common/bin/dunecontrol --opts=config.opts all

# install all python modules in the pip environment
./dune-python/bin/setup-dunepy.py --opts=config.opts install

# get dune-pdelab-tutorials and run cmake only
for MOD in dune-pdelab-tutorials; do
  $GITCLONE $DUNEBRANCH https://gitlab.dune-project.org/pdelab/$MOD.git
  ./dune-common/bin/dunecontrol --opts=config.opts --only=$MOD cmake
done

# compile some grids for python
python dune-fempy/doc/compilegrids.py 2
python dune-fempy/doc/compilegrids.py 3
##################################################################

cd ..
