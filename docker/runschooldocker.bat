@echo off

set dockerName=registry.dune-project.org/smuething/inspire-summer-school/school:latest
set dockerName=alpine
set userName=school

echo On Windows: for X forwarding you will need an x-seerver app running use for example vcxsrv
echo start it with "xlaunch" and tick "disable access control"
echo.
echo also note that to run this image docker must be configured to have access to at least 4GB of memory
echo this can be set in the 'advanced' tab of the docker menu.
echo Changing the maximal number of cores can also increase performance:
echo.


if NOT EXIST winnfsd.exe (
  echo This container requires winnfsd to share files between the host and the container
  echo Please place winnfsd.exe in this directory
  echo https://github.com/winnfsd/winnfsd/releases
  pause
)

REM Start NFS server
echo %cd% ^> /host > exports.txt
echo %cd%\inspire ^> /inspire >> exports.txt

md inspire

REM The NFS server needs to run with elevated privileges, otherwise it can't create symlinks

echo **********************************************************************
echo Starting NFS server, please allow elevated access
echo The NFS server will run in a separate window.
echo - DO NOT close this window while the Docker container is running.
echo - You can reduce the amount of log output by typing "log off" in the
echo   server window.
echo - You can stop the server by typing "quit" in the server window.
echo **********************************************************************

pause
powershell -Command "Start-Process -filePath '.\WinNFSd.exe' -ArgumentList '-pathFile','%cd%\exports.txt' -Verb runas"

echo Checking whether host volume exists ...
for /f %%i in ('docker volume ls -qf name^=host') do set hostvol=%%i

if NOT "%hostvol%" == "host" (
  echo Creating NFS volume for /host ...
  docker volume create --name host --opt type=nfs --opt device=:/host --opt o=addr=host.docker.internal
)

:handleinspire

if EXIST INSPIRE_EXPORTED_TO_HOST (
  echo Mounting exported /inspire directory from host
  echo Checking whether inspire volume exists ...
  for /f %%i in ('docker volume ls -qf name^=inspire') do set inspirevol=%%i
  if NOT "%inspirevol%" == "inspire" (
    echo Creating NFS volume for /inspire ...
    docker volume create --name inspire --opt type=nfs --opt device=:/inspire --opt o=addr=host.docker.internal
  )
  goto :inspirehandled
)

if EXIST INSPIRE_ONLY_IN_CONTAINER (
  echo /inspire is only available inside the container
  goto :inspirehandled
)

echo ***********************************************************************
echo Do you want the C++ source code of all modules to be available on your
echo host (directly in Windows)?"
echo NOTE: This is slower than keeping the files inside the container, but
echo       allows you to browse and edit the files and outputs using your
echo       normal Windows tools.
echo ***********************************************************************

:tryinput
set /p decision= Do you want to copy the files to Windows? (y/n)

echo %decision%

if "%decision%" == "y" (
  echo ok > INSPIRE_EXPORTED_TO_HOST
  goto :handleinspire
)

if "%decision%" == "n" (
  echo ok > INSPIRE_ONLY_IN_CONTAINER
  goto :handleinspire
)

goto :tryinput


:inspirehandled

docker run -it --rm -v host:/host -v %userName%:/%userName% -v inspire:/inspire --name "%userName%" -e DISPLAY=host.docker.internal:0 --privileged -e userId=2000 -e groupId=2000 %dockerName%
