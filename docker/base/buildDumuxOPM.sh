#!/bin/bash
# One click install script dumux

# Make sure we are running in Bash
if [ -n "$(type -p)" ] ; then
    exec /bin/bash $0 "$@"
fi

# basic setup
source $HOME/dune-env/bin/activate
export DUNE_CONTROL_PATH=$HOME/DUNE:.
export PATH=$PATH:$HOME/DUNE/dune-common/bin

cd ~/DUNE

# only clone with the last commit
GITCLONE="git clone --depth=50"

# check some prerequistes
for PRGRM in git git-lfs cmake gcc g++ wget pkg-config gnuplot; do
    if ! [ -x "$(command -v $PRGRM)" ]; then
        echo "Error: $PRGRM is not installed." >&2
        exit 1
    fi
done

# check some library prerequistes
for LIBRARY in libumfpack; do
    if ! [ "$(/sbin/ldconfig -p | grep $LIBRARY)" ]; then
        echo "Error: $LIBRARY is not installed." >&2
        exit 1
    fi
done

currentver="$(gcc -dumpversion)"
requiredver="4.9.0"
if [ "$(printf '%s\n' "$requiredver" "$currentver" | sort -V | head -n1)" != "$requiredver" ]; then
    echo "gcc greater than or equal to $requiredver is required!" >&2
    exit 1
fi

echo "*********************************************************************************************"
echo "(1/2) Cloning repositories. This may take a while. Make sure to be connected to the internet."
echo "*********************************************************************************************"
# the core modules
#for MOD in common geometry grid localfunctions istl; do
#    if [ ! -d "dune-$MOD" ]; then
#        $GITCLONE https://gitlab.dune-project.org/core/dune-$MOD.git
#    else
#        echo "Skip cloning dune-$MOD because the folder already exists."
#    fi
#done

# dumux
if [ ! -d "dumux" ]; then
    $GITCLONE -b releases/3.0 https://git.iws.uni-stuttgart.de/dumux-repositories/dumux.git
else
    echo "Skip cloning dumux because the folder already exists."
fi

# ecl and opm-grid
if [ ! -e libecl ]; then
    $GITCLONE https://github.com/Equinor/libecl.git
fi

# building libecl
cd libecl
mkdir build
cd build
cmake ..
make -j${CORES}

cd ..
cd ..

OPM_MODULES="opm-common opm-grid opm-material"

for MOD in $OPM_MODULES; do
  if [ ! -e $MOD ]; then
    $GITCLONE -b release/2019.04 https://github.com/OPM/$MOD
    ./dune-common/bin/dunecontrol --opts=$HOME/DUNE/config.opts --only=$MOD all
  fi
done

OPM_SIM_MODS="ewoms"
for MOD in $OPM_SIM_MODS; do
  if [ ! -e $MOD ]; then
    $GITCLONE -b release/2019.04-patched https://github.com/dr-robertk/$MOD
    ./dune-common/bin/dunecontrol --opts=$HOME/DUNE/config.opts --only=$MOD all
  fi
done

if [ $? -ne 0 ]; then
    echo "*********************************************************************************************"
    echo "Failed to clone the repositories."
    echo "*********************************************************************************************"
    exit $?
fi

echo "*********************************************************************************************"
echo "(2/2) Configure dune modules and dumux. Build the dune libaries. This may take several minutes."
echo "*********************************************************************************************"
# run build
./dune-common/bin/dunecontrol --opts=$HOME/DUNE/config.opts --only=dumux all
#./dune-common/bin/dunecontrol --opts=$HOME/DUNE/config.opts --only=dumux-course all
#./dune-common/bin/dunecontrol --opts=$HOME/DUNE/config.opts --only=dumux-lecture all

#./dune-common/bin/dunecontrol --opts=$HOME/DUNE/config.opts --module=dumux-course all
#./dune-common/bin/dunecontrol --opts=$HOME/DUNE/config.opts --module=opm-simulators all
#
if [ $? -ne 0 ]; then
    echo "*********************************************************************************************"
    echo "Failed to build the dune libaries."
    echo "*********************************************************************************************"
    exit $?
fi

# echo result
echo "*********************************************************************************************"
echo "Succesfully configured and built dune and dumux."
echo "Please change to the dumux folder and run the test_dumux.sh script to confirm everything works."
echo "*********************************************************************************************"

cd $HOME/tutorials

# dumux-course
if [ ! -d "dumux-course" ]; then
    $GITCLONE -b releases/3.0 https://git.iws.uni-stuttgart.de/dumux-repositories/dumux-course.git
else
    echo "Skip cloning dumux-course because the folder already exists."
fi

# dumux-lecture
if [ ! -d "dumux-lecture" ]; then
    $GITCLONE -b releases/3.0 https://git.iws.uni-stuttgart.de/dumux-repositories/dumux-lecture.git
else
    echo "Skip cloning dumux-lecture because the folder already exists."
fi
OPM_SIM_MODS="opm-simulators"
for MOD in $OPM_SIM_MODS; do
  if [ ! -e $MOD ]; then
    $GITCLONE -b release/2019.04-patched https://github.com/dr-robertk/$MOD
    # ./dune-common/bin/dunecontrol --opts=$HOME/DUNE/config.opts --only=$MOD all
  fi
done

for MOD in opm-data; do
  if [ ! -e $MOD ]; then
    $GITCLONE https://github.com/OPM/$MOD
  fi
done
