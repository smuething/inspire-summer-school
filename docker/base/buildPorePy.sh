#!/bin/bash

# Should be done externally:
# 1)  run sudo apt install python3-tk
#    I could not get that install to work with pip or otherwise
# 2) Make sure the directory specified by variable $DIR_IN_PYTHONPATH
#    is indeed in the pythonpath (in .bashrc)
# 3) sudo apt install python3-dev

## Gmsh should be available in version >=4.0
# If true, gmsh is installed in directory specified below
DO_INSTALL_GMSH=$true

cd $HOME

OLD_DIR=`pwd`

# Directory for gmsh install, if $DO_INSTALL_GMSH
#GMSH_DIR="$OLD_DIR/gmsh"

if ! test -x /usr/local/bin/gmsh; then
#if ! test -d "$GMSH_DIR" ; then
  wget http://gmsh.info/src/gmsh-4.4.0-source.tgz
  tar zxf gmsh-4.4.0-source.tgz
  cd gmsh-4.4.0-source
  mkdir build
  cd build
  cmake ../
  make -j8
  sudo make install
  cd $OLD_DIR
  rm -rf gmsh-4.4.0-source
  rm -f gmsh-4.4.0-source.tgz
#fi
fi

# Base directory for the install
BASE_DIR="$HOME/PorePy"

if ! test -d "$BASE_DIR"; then
  mkdir $BASE_DIR
fi

cd $BASE_DIR

# Path to the gmsh binary.
GMSH_RUN="gmsh"

# Whether to download porepy or not. If true, any directory
# with the name $POREPY_DIR (below) will be deleted
DO_DOWNLOAD_POREPY=$true

# Install directory for porepy
POREPY_DIR="$BASE_DIR/porepy"

# Give a directory in $PYTHONPATH
# if the directory does not exist, it will be made
DIR_IN_PYTHONPATH="$BASE_DIR/python"
export PYTHONPATH=$PYTHONPATH:$BASE_DIR/python

# Make a virtual env dune-env
if ! test -d $HOME/dune-env ; then
  python3.6 -m venv $HOME/dune-env
  source $HOME/dune-env/bin/activate

  pip install --upgrade pip
else
  source $HOME/dune-env/bin/activate
fi

# Delete porepy directory if it exists (not sure if this is good practice)
if $DO_DOWNLOAD_POREPY ; then
  if ! test -d "$POREPY_DIR" ; then
    rm -Rf $POREPY_DIR;
  fi
  # Clone PorePy from github
  git clone https://github.com/pmgbergen/porepy.git $POREPY_DIR
fi



# Install porepy requirements
pip install -r "$POREPY_DIR/requirements-dev.txt"

# install additional packages
pip install numba vtk jupyter ipython

# install porepy
pip install $POREPY_DIR

if ! test -d $DIR_IN_PYTHONPATH ; then
  mkdir ${DIR_IN_PYTHONPATH}
fi

# Write the path to the gmsh binary to a file porepy_config.py
echo "config = {\"gmsh_path\": \"$GMSH_RUN\" } " > $DIR_IN_PYTHONPATH/porepy_config.py

# Install a file from GitHub which is not available through pip or other sources
wget 'https://raw.githubusercontent.com/keileg/polyhedron/master/polyhedron.py'
mv polyhedron.py $DIR_IN_PYTHONPATH/robust_point_in_polyhedron.py

# Finally, run tests
cd $POREPY_DIR
pytest test/unit test/integration

cd $BASE_DIR

cd $OLD_DIR

