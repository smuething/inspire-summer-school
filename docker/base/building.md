# How to Update the Official Base Image

To update the official Docker image on `registry.dune-project.org`,
please follow these steps:

1. Actually build the docker image:
    ```
    docker build . --no-cache -t registry.dune-project.org/dune-fem/dune-fempy-base:latest
    ```
    Please do not omit `--no-cache` to ensure every step of the build is actually
    executed.

    *Note*: This will set up the Ubuntu base system of the Docker image, which takes
    quite some time.

1. Verify your build by testing the Docker image locally by switching to
   the main directory and following the instructions in the README.md file

1. Log into the Docker registry:
    ```
    docker login registry.dune-project.org
    ```

1. Push the Docker image:
    ```
    docker push registry.dune-project.org/dune-fem/dune-fempy-base:latest
    ```
    This step actually updates the Docker image on the server.

    **Warning:** Updating the official Docker image affects all users of the
    image once they pull it.
    Make sure you don't push a buggy combination of the DUNE modules.

1. Log out of the Docker registry:
    ```
    docker logout registry.dune-project.org
    ```

That's it.
