#!/usr/bin/env bash

if [ ! $groupId -eq 0 ] && [ ! $userId -eq 0 ] ; then
  # echo "switching user:group id from `id -u dune`:`id -g dune` to $userId:$groupId"
  chgrp -R -h $groupId /school
  chown -R -h $userId /school
  usermod -u $userId dune
  groupmod -o -g $groupId dune
  chgrp -R -h $groupId /inspire
  chown -R -h $userId /inspire
  echo "################################################################"
  echo "# Welcome to the Geilo school docker development environment.  #"
  echo "# The Dune git repositories of upstream modules  are located   #"
  echo "# at '/school/DUNE', some of the C++-based tutorials are in    #"
  echo "# '/schools/tutorials'.                                        #"
  echo "# A python virtual environment can be activated by running     #"
  echo "# 'load_school' and Python scripts can be found in             #"
  echo "# '/school/DUNE/dune-fempy/docs'. Also run this command before #"
  echo "# working with the contents of '/schools/tutorials'.           #"
  echo "# The remaining tutorials have been copied to your host so     #"
  echo "# that you can inspect and edit the sources with your normal   #"
  echo "# tools on your host computer.  Those files can be found in    #"
  echo "# the directory '/inspire' inside the Docker container         #"
  echo "# (use for compiling and running) and in the subdirectory      #"
  echo "# 'inspire' of the directory where you ran this script on your #"
  echo "# host computer.                                               #"
  echo "################################################################"
  exec gosu dune bash
else
  # echo "not setting any user/group id - will start as root"
  exec bash
fi
