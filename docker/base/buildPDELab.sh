#!/bin/bash

# Make sure we are running in Bash
if [ -n "$(type -p)" ] ; then
    exec /bin/bash $0 "$@"
fi

cd /inspire
# clone into current directory
git clone https://gitlab.dune-project.org/dune-course/inspire-school-2019.git .
# build
./install.sh
# move to home directory (to allow for creation of shared volume)
cd $HOME
mkdir -p $HOME/inspire
( shopt -s dotglob nullglob ; mv /inspire/* $HOME/inspire )
echo "
if [ ! -d /inspire/dune ] ; then
  echo \"Setting up summer school project in directory /inspire\"
  echo \"This might take a few minutes, especially if you made the files available to the host on Windows...\"
  ( shopt -s dotglob nullglob ; mv \$HOME/inspire/* /inspire )
fi
" >> $HOME/.bashrc
