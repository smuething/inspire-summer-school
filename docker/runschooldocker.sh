#!/usr/bin/env bash

dockerName=registry.dune-project.org/smuething/inspire-summer-school/school:latest
userName=school

# Is Docker installed?
haveDocker=$(docker -v)
if [ ! $? -eq 0 ];
then
  echo "Docker could not be found on your system."
  echo "Please install docker following instructions for you operating system from"
  echo "https://docs.docker.com."
  exit 1
fi


# check operating system and start docker
if [ $(uname) = "Linux" ] ;
then
  # need to check if docker is called using 'sudo'
  if [ ! "$SUDO_UID" = "" ] ;
  then
    USERID=$SUDO_UID
  else
    USERID=$(id -u)
  fi
  if [ ! "$SUDO_GID" = "" ] ;
  then
    GROUPID=$SUDO_GID
  else
    GROUPID=$(id -g)
  fi

  # now start docker container
  xhost +si:localuser:$USER
  if [ ! "$(docker container ls -a | grep $userName)" ] ;
  then
    docker run -it --name $userName -v $PWD:/host -v $userName:/$userName \
      -v /tmp/.X11-unix:/tmp/.X11-unix:ro --device /dev/dri \
      -v $PWD/inspire:/inspire \
      -e userId=$USERID -e groupId=$GROUPID --hostname="$userName" --add-host $userName:127.0.0.1 $dockerName
  else
    docker start -i $userName
  fi
  xhost -si:localuser:$USER
elif [ $(uname) = "Darwin" ] ;
then
  echo "on MAC: for X forwarding remember to run"
  echo '    socat TCP-LISTEN:6000,reuseaddr,fork UNIX-CLIENT:\"$DISPLAY\"'
  echo "in separate terminal"
  echo ""
  echo "also note that to run this image docker must be configured to have access to at least 4GB of memory"
  echo "this can be set in the 'advanced' tab of the docker toolbar UI; another option is to run"
  echo "docker-machine stop"
  echo "VBoxManage modifyvm default --memory 4096"
  echo "docker-machine start"
  echo "Changing the maximal number of cores can also increase performance:"
  echo "VBoxManage modifyvm default --cpus 4"
  echo ""
  xhost +si:localuser:$USER
  if [ ! "$(docker container ls -a | grep $userName)" ] ;
  then
    docker run -it -v $PWD:/host:delegated -v $userName:/$userName --name $userName \
      -v /tmp/.X11-unix:/tmp/.X11-unix:ro \
      -v $PWD/inspire:/inspire:delegated \
      -e DISPLAY=$(ipconfig getifaddr en0):0 --net=host \
      -e userId=$(id -u) -e groupId=$(id -g) --hostname="$userName" --add-host $userName:127.0.0.1 $dockerName
  else
    docker start -i $userName
  fi
  xhost -si:localuser:$USER
elif [ $(uname) = "MINGW64_NT-10.0" ] ;
then
  # https://dev.to/darksmile92/run-gui-app-in-linux-docker-container-on-windows-host-4kde
  echo "On Windows: for X forwarding you will need an x-seerver app running use for example vcxsrv -"
  echo "start it with \"xlaunch\" and tick \"disable access control\""
  echo ""
  echo "also note that to run this image docker must be configured to have access to at least 4GB of memory"
  echo "this can be set in the 'advanced' tab of the docker menu."
  echo "Changing the maximal number of cores can also increase performance:"
  echo ""

  # configure and start NFS server
  echo "$PWD > /host" > exports.txt
  echo "$PWD/inspire > /host" > exports.txt

  echo "**********************************************************************"
  echo "Starting NFS server, please allow elevated access"
  echo "The NFS server will run in a separate window."
  echo "- DO NOT close this window while the Docker container is running."
  echo "- You can reduce the amount of log output by typing "log off" in the"
  echo "  server window."
  echo "- You can stop the server by typing "quit" in the server window."
  echo "**********************************************************************"

  read -n1 -r -p "Press any key to continue..." key

  powershell -Command "Start-Process -filePath '.\WinNFSd.exe' -ArgumentList '-pathFile','exports.txt' -Verb runas"

  ipaddr=$(ipconfig | grep "IPv4" | head -1 | grep -oE '[^ ]+$')

  echo "Removing old host volume..."
  docker volume remove host
  echo "Creating new shared host volume..."
  docker volume create --name=host --opt type=nfs --opt device=:/host --opt o=addr=$ipaddr

  if [ -f INSPIRE_EXPORTED_TO_HOST ] ; then
    echo "Mounting exported /inspire directory from host"
    echo "Removing old inspire volume..."
    docker volume remove inspire
    echo "Creating new shared inspire volume..."
    docker volume create --name=inspire --opt type=nfs --opt device=:/inspire --opt o=addr=$ipaddr
  elif [ -f INSPIRE_ONLY_IN_CONTAINER ] ; then
    echo "/inspire is only available inside the container"
  else
    echo "***********************************************************************"
    echo "Do you want the C++ source code of all modules to be available on your"
    echo "host (directly in Windows)?"
    echo "NOTE: This is slower than keeping the files inside the container, but"
    echo "      allows you to browse and edit the files and outputs using your"
    echo "      normal Windows tools."
    echo "***********************************************************************"

    while read -r -p "Do you want to copy the files to Windows? (y/n)" response ; do
      case $response in
        y )
          echo "Mounting exported /inspire directory from host"
          touch INSPIRE_EXPORTED_TO_HOST
          echo "Creating new shared inspire volume..."
          docker volume create --name=inspire --opt type=nfs --opt device=:/inspire --opt o=addr=$ipaddr
          break
          ;;
        n )
          echo "/inspire is only available inside the container"
          touch INSPIRE_ONLY_IN_CONTAINER
          ;;
        esac
    done
  fi

  if [ -z "$DISPLAY" ] || [ "$DISPLAY" == "needs-to-be-defined" ];
  then
    export DISPLAY=$(ipconfig | grep "IPv4" | head -1 | grep -oE '[^ ]+$'):0
    echo "Setting DISPLAY to $DISPLAY"
  fi

  docker run -it --rm -v "$PWD":/host -v $userName:/$userName -v inspire:/inspire --name="$userName" \
         -e DISPLAY=$DISPLAY --privileged \
         -e userId=$(id -u) -e groupId=$(id -g) $dockerName

else
  echo "System not tested on your architecture identified as $uname"
fi
