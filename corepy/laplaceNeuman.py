import numpy, math
import scipy.sparse
from scipy.sparse.linalg import spsolve
import dune.geometry
from dune.grid import cartesianDomain, gridFunction
from dune.alugrid import aluConformGrid

# Base class for discrete space:
# the base class is constructed with
# - the grid view
# - the local number of degrees of freedom
# - the dof layout per element
# Note: at the moment a non hybrid grid is assumed
# The base class provides:
# - __len__: number of global degrees of freedom
# - map(element): returns a list of indices on a given element
# - interpolate(element): returns the dofs of the natural interpolation of
#                         a 'bound' local function
# - function(dofVector): returns a grid function representing the discrete
#                    function with the given dof vector
# Derived classes need to implement:
# - evaluateLocal(x): evaluate shape functions in local coordinate
# - gradientLocal(x): gradients of shape functions in local coordinate
class LagrangeSpace:
    def __init__(self,view,localDofs,layout):
        self.view   = view
        self.dim    = view.dimension
        self.mapper = view.mapper(layout)
        self.localDofs = localDofs
    # number of global degrees of freedom
    def __len__(self):
        return len(space.mapper)
    def map(self,element):
        return self.mapper(element)
    def interpolate(self,lgf):
        return lgf(self.points.transpose())
    def function(self,dofVector):
        @gridFunction(view)
        def uh(e,x):
            indices = self.map(e)
            phiVals = self.evaluateLocal(x)
            localDofs = dofVector[indices]
            return numpy.dot(localDofs, phiVals)
        return uh
class LinearLagrangeSpace(LagrangeSpace):
    def __init__(self,view):
        LagrangeSpace.__init__(self,view,3,
                       lambda gt: 1 if gt.dim == 0 else 0)
        self.points = numpy.array( [ [0,0],[1,0],[0,1] ] )
    def evaluateLocal(self, x):
        bary = 1.-x[0]-x[1], x[0], x[1]
        return numpy.array( bary )
    def gradientLocal(self, x):
        bary  = 1.-x[0]-x[1], x[0], x[1]
        dbary = [[-1.,-1],[1.,0.],[0.,1.]]
        return numpy.array( dbary )
class QuadraticLagrangeSpace(LagrangeSpace):
    def __init__(self,view):
        LagrangeSpace.__init__(self,view,6,
                    lambda gt: 1 if gt.dim == 0 or gt.dim == 1 else 0)
        self.points = numpy.array( [ [0,0],[1,0],[0,1],
                                   [0.5,0],[0,0.5],[0.5,0.5] ] )
    def evaluateLocal(self, x):
        bary = 1.-x[0]-x[1], x[0], x[1]
        return numpy.array([ bary[i]*(2.*bary[i]-1.) for i in range(3) ] +\
               [ 4.*bary[(3-j)%3]*bary[(4-j)%3] for j in range(3) ])
    def gradientLocal(self, x):
        bary  = 1.-x[0]-x[1], x[0], x[1]
        dbary = [[-1.,-1],[1.,0.],[0.,1.]]
        return numpy.array([ [ dbary[i][0]*(2.*bary[i]-1.)+bary[i]*2.*dbary[i][0],
                   dbary[i][1]*(2.*bary[i]-1.)+bary[i]*2.*dbary[i][1] ]
                for i in range(3) ] +\
               [ [ 4.*dbary[(3-j)%3][0]*bary[(4-j)%3]+ 4.*bary[(3-j)%3]*dbary[(4-j)%3][0],
                   4.*dbary[(3-j)%3][1]*bary[(4-j)%3]+ 4.*bary[(3-j)%3]*dbary[(4-j)%3][1] ]
                for j in range(3) ])

def assemble(space,force,mass=1):
    # storage for right hand side
    rhs = numpy.zeros(len(space))

    # storage for local matrix
    localEntries = space.localDofs
    localMatrix = numpy.zeros([localEntries,localEntries])

    # data structure for global matrix using COO format
    globalEntries = localEntries**2 * space.view.size(0)
    value = numpy.zeros(globalEntries)
    rowIndex, colIndex = numpy.zeros(globalEntries,int), numpy.zeros(globalEntries,int)

    # iterate over grid and assemble right hand side and system matrix
    count = 0
    for e in view.elements:
        geo = e.geometry
        indices = space.map(e)
        localMatrix.fill(0)
        for p in dune.geometry.quadratureRule(e.type, 4):
            x = p.position
            w = p.weight * geo.integrationElement(x)
            phiVals = space.evaluateLocal(x)
            rhs[indices] += w * force(e,x) * phiVals[:]

            vals  = space.evaluateLocal(x)
            jit   = numpy.array(geo.jacobianInverseTransposed(x), copy=False)
            grads = [numpy.dot(jit, dphi) for dphi in space.gradientLocal(x)]
            for i, [phi, dphi] in enumerate( zip(vals,grads) ):
                for j, [psi, dpsi] in enumerate( zip(vals,grads) ):
                    localMatrix[i,j] += mass*numpy.dot( phi,  psi) * w
                    localMatrix[i,j] += numpy.dot(dphi, dpsi) * w

        # store indices and local matrix for COO format
        indices = space.map(e)
        for i in range(localEntries):
            for j in range(localEntries):
                value[count]    = localMatrix[i,j]
                rowIndex[count] = indices[i]
                colIndex[count] = indices[j]
                count += 1

    matrix = scipy.sparse.coo_matrix((value, (rowIndex, colIndex)),
                         shape=(len(space),len(space))).tocsr()

    return rhs,matrix

def error(u,uh):
    @gridFunction(view)
    def error(e,x):
        return (uh(e,x)-u(e,x))**2
    rules = dune.geometry.quadratureRules(4)
    ret = 0
    for e in view.elements:
        ret += dune.geometry.integrate(rules,e,error)
    ret = math.sqrt(ret)
    return ret

domain = cartesianDomain([0, 0], [1, 1], [10, 10])
view   = aluConformGrid(domain)

mass = 1.
@gridFunction(view)
def u(p):
    x,y = p
    return numpy.cos(2*numpy.pi*x)*numpy.cos(2*numpy.pi*y)
@gridFunction(view)
def forcing(p):
    return u(p)*((2*numpy.pi)**2*2+mass)
u.plot(level=3)

for step in range(3):
    space = QuadraticLagrangeSpace(view)
    rhs,matrix = assemble(space, forcing, mass)
    dofs = spsolve(matrix,rhs)
    uh = space.function(dofs)
    print(view.size(0), error(u,uh))
    uh.plot(level=1)
    view.hierarchicalGrid.globalRefine(2)
