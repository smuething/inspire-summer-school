## Basic grid construction, usage, and plotting

# 1st
from dune.grid import structuredGrid
view = structuredGrid([-0.5,-0.5],[2,1],[10,20])
view.plot()

# 2nd
from dune.alugrid import aluSimplexGrid
mesh = { "vertices":[ [ 0, 0],
                      [ 1, 0],
                      [ 1, 1],
                      [-1, 1],
                      [-1,-1],
                      [ 0,-1] ],
         "simplices": [ [0,1,2],
                        [0,2,3],
                        [0,3,4],
                        [0,4,5] ] }
view = aluSimplexGrid(constructor=mesh)
view.plot()

# 3rd
for element in view.elements:
    geo = element.geometry
    print(geo.center,geo.volume)

# 4th
from dune.grid import gridFunction
from math import sin, log, pi
@gridFunction(view)
def f(element,localx):
    y = element.geometry.toGlobal(localx)
    return sin(20*y[0]*y[1])*y.two_norm
f.plot()

# 5th
f.plot(level=5)

# 6th
view.hierarchicalGrid.globalRefine(3)
f.plot(gridLines="white",level=2)

# 7th
view = structuredGrid([-0.5,-0.5],[2,1],[10,20])
@gridFunction(view)
def f(y):
    return sin(20*y[0]*y[1])*y.two_norm
f.plot()

# 8th
view.writeVTK("interpolation", pointdata={"sin":f})

## Attaching data to the grid, i.e., vertices
# 1st
view = aluSimplexGrid(constructor=mesh)
view.hierarchicalGrid.globalRefine(3)
@gridFunction(view)
def f(y):
    return sin(2*y[0]*y[1])*y.two_norm
from dune.geometry import vertex
import numpy
def interpolate():
    mapper = view.mapper({vertex: 1})
    data = numpy.zeros(mapper.size)
    for v in view.vertices:
        data[mapper.index(v)] = f(v.geometry.center)
    return mapper,data

mapper, data = interpolate()

@gridFunction(view)
def p12dEvaluate(e, x):
    bary = 1-x[0]-x[1], x[0], x[1]
    idx = mapper.subIndices(e, 2)
    return sum(b * data[i] for b, i in zip(bary, idx))

p12dEvaluate.plot(figsize=(9,9), gridLines=None)

# 2nd
@gridFunction(view)
def error(e, x):
    return abs(p12dEvaluate(e, x)-f(e,x))
hatx = [1./2., 1./2.]
err = max(error(e, hatx) for e in view.elements)
print(view.size(0), err)
for i in range(4):
    oldErr = err
    view.hierarchicalGrid.globalRefine(1)
    mapper, data = interpolate()
    err = max(error(e, hatx) for e in view.elements)
    eoc = log(oldErr/err)/log(2)
    print(view.size(0), err, eoc)

p12dEvaluate.plot(figsize=(9,9), gridLines=None)

# 3rd
from dune.grid import cartesianDomain
domain = cartesianDomain([0, 0], [1, 1], [3, 2])
view = aluSimplexGrid(domain)
mapper = view.mapper(lambda gt: 2 if gt.dim == 0 or gt.dim == 1 else 0)
for element in view.elements:
    print("all indices on element:", mapper(element))
    # this method is not available in C++ there one uses for example:
    # std::vector<unsigned int> idx;
    # for (unsinged int i=0;i<3;++i)
    # {
    #   idx.push_back(mapper.subIndex(element, i, 2));
    #   idx.push_back(mapper.subIndex(element, i, 2)+1);
    # }
    # for (unsinged int i=0;i<3;++i)
    # {
    #   idx.push_back(mapper.subIndex(element, i, 1));
    #   idx.push_back(mapper.subIndex(element, i, 1)+1);
    # }
    # which will result in the same index vector
## Quadrature
# 1st simple direct
from dune.grid import cartesianDomain
from dune.geometry import quadratureRule, quadratureRules, integrate

domain = cartesianDomain([0, 0], [1, 1], [50, 50])
view = aluSimplexGrid(domain)
@gridFunction(view)
def f(x): return sin(3*pi*x[0])*sin(3*pi*x[1])

value = 0
for e in view.elements:
    geo = e.geometry
    for p in quadratureRule(e.type, 4):
        x = p.position
        w = p.weight * geo.integrationElement(x)
        y = geo.toGlobal(x)
        value += w * f(y)
print("integral:", value)

#2nd C++ version
value = 0
lf = f.localFunction()
for e in view.elements:
    lf.bind(e)
    geo = e.geometry
    for p in quadratureRule(e.type, 4):
        x = p.position
        w = p.weight * geo.integrationElement(x)
        value += w * lf(x)
    lf.unbind()
print("integral:", value)
from dune.generator import algorithm
value = algorithm.run('integrate', 'integrate.hh', view, f, 4)
print("integral:", value)

#3nd: vectorized
@gridFunction(view)
def f(x): return numpy.sin(3*pi*x[0])*numpy.sin(3*pi*x[1])

value = 0
rules = quadratureRules(4)
for e in view.elements:
    value += integrate(rules,e,f)
print("integral:",value)
