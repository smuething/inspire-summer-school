# For the lectures
  - firstexample.py:
    basic grid io and usage
  - secondexample.py:
    implementation of a L^2 projection into a linear Lagrange space
# For the practicals
## Python
### Part 1 as shown in laplaceNeuman.py:
    - compute errors and eocs for L^2 projection
    - extend L^2 projection to Laplace problem
    - quadratic finite elements
### Part 2 as shown in laplace.py:
    add dirichlet boundary conditions
## For the practicals (C++)
  - implement the rhs/matrix assembly completely in C++
    using generator.algorithm
## Extensions for the keen
  - extend to spiral wave model
    (see http://www.scholarpedia.org/article/Barkley_model)

# What needs to be done
## Part 1:
- implement an error function to compute |u-ui\_h|\_L^2
- implement a class QuadraticLagrangeSpace(LagrangeSpace) following the
  LinearLagrangeSpace example
- add a 'mass' argument to 'def assemble(space,force)'
                           'def assemble(space,force,mass=1)'
  and implement stiffness matrix, i.e.,
            localMatrix[i,j] += mass*numpy.dot( phi,  psi) * w + numpy.dot(dphi, dpsi) * w
- implement right hand side for laplace problem:
  `u(p)*((2*numpy.pi)**2*2+mass)`
## Part 2:
- Add methods to discrete spaces:
  Base::onIntersection(intersection):
  returns the local indices of all dofs attached to given intersection
  onSubEntity(subEntity):
  return the degrees of freedom attach to given subEntity
- add argument to 'assemble' to pass in dirichlet data
- iterate over all boundary intersections and
  - collect all global indices of dofs on the intersection (e.g. using a 'set')
  - interpolate the dirichlet data on inside entity
  - set the rhs dofs using the interpolated values and the global/local indices
- modify the system matrix using the dirichlet indices I by
  - constructing a vector idInner with 1 for i in I and 0 otherwise
  - multiplying system matrix with diag(idInner)
         dia_matrix((idInner,0),shape=(len(space),len(space)))
             .tocsr().dot(matrix)
  - constructing a vector idOuter with 1 for i not in I and 0 otherwise
  - adding diag(idOuter) to system matrix
         matrix += dia_matrix((idOuter,0),shape=(len(space),len(space))).tocsr()
